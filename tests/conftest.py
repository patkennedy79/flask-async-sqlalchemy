import pytest
from app import app
from models import database


############
# Fixtures #
############

@pytest.fixture(scope='module')
def test_client():
    # Create a test client using the Flask application
    with app.test_client() as testing_client:
        # Establish an application context before accessing the logger and database
        with app.app_context():
            app.logger.info('Creating database tables in test_client fixture...')

            # Create the database and the database table(s)
            database.create_all()

        yield testing_client  # this is where the testing happens!

        with app.app_context():
            database.drop_all()

