"""
This file (test_app.py) contains the functional tests for *app.py*.
"""
import pytest
import aiohttp
from app import get_all_urls, fetch_url


def test_get_homepage(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Hello World!' in response.data


def test_async_get_urls(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/async_get_urls' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/async_get_urls')
    assert response.status_code == 200
    assert b'URLs' in response.data


def test_async_list(test_client):
    """
    GIVEN a Flask test client
    WHEN the '/async_list' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/async_list_urls')
    assert response.status_code == 200
    assert b'Database Items' in response.data


@pytest.mark.asyncio
async def test_get_all_urls(test_client):
    results = await get_all_urls()
    assert len(results) > 0
    print(type(results))
    for item in results:
        assert 'www.kennedyrecipes.com' in str(item['url'])
        assert int(item['status']) == 200


@pytest.mark.asyncio
async def test_fetch_url(test_client):
    async with aiohttp.ClientSession() as session:
        result = await fetch_url(session, 'https://www.kennedyrecipes.com/baked_goods/bagels/')

    assert str(result['url']) == 'https://www.kennedyrecipes.com/baked_goods/bagels/'
    assert int(result['status']) == 200
