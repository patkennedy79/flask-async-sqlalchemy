from flask import Flask
import asyncio
from aiohttp import ClientSession
from timeit import default_timer
from ssl import create_default_context
from certifi import where
from models import database, Site
from sqlalchemy.future import select
from functools import wraps


# Create the Flask application
app = Flask(__name__)

# Create a SSL context to use with aiohttp
sslcontext = create_default_context(cafile=where())

# List of (10) websites to access in this script
urls = ['https://www.kennedyrecipes.com',
        'https://www.kennedyrecipes.com/breakfast/pancakes/',
        'https://www.kennedyrecipes.com/breakfast/honey_bran_muffins/',
        'https://www.kennedyrecipes.com/breakfast/acai_bowl/',
        'https://www.kennedyrecipes.com/breakfast/breakfast_scramble/',
        'https://www.kennedyrecipes.com/breakfast/pumpkin_donuts/',
        'https://www.kennedyrecipes.com/breakfast/waffles/',
        'https://www.kennedyrecipes.com/breakfast/omelette/',
        'https://www.kennedyrecipes.com/baked_goods/bagels/',
        'https://www.kennedyrecipes.com/baked_goods/irish_soda_bread/']


##############
# Decorators #
##############

def timer(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        start_time = default_timer()
        response = f(*args, **kwargs)
        total_elapsed_time = default_timer() - start_time
        response += f'<h3>Elapsed time: {total_elapsed_time}</h3>'
        return response
    return wrapper


####################
# Helper Functions #
####################

async def fetch_url(session, url):
    """Fetch the specified URL using the aiohttp session specified."""
    response = await session.get(url, ssl=sslcontext)
    return {'url': response.url, 'status': response.status}


async def get_all_urls():
    """Retrieve the list of URLs and store their status in the database."""

    # Retrieve all the URLs asynchronously using aiohttp
    async with ClientSession() as session:
        tasks = []
        for url in urls:
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        results = await asyncio.gather(*tasks)

    sites = []
    for item in results:
        sites.append(Site(str(item['url']), int(item['status'])))

    # Store the URL responses (URL, status code) asynchronously in the database
    async with database.session as database_session:
        async with database_session.begin():
            database_session.add_all(sites)
        await database_session.commit()

    return results


async def async_get_sites():
    """Retrieve all the URL responses asynchronously that are stored in the database."""
    async with database.session as database_session:
        sites = await database_session.execute(select(Site).order_by(Site.id))
        sites_all = await database_session.execute(select(Site))
        sites_valid_status = await database_session.execute(select(Site).filter_by(status=200))

    return {'sites': sites,
            'sites_count': len(sites_all.all()),
            'sites_valid_count': len(sites_valid_status.all())}


######################
#### cli commands ####
######################

@app.cli.command('create_tables')
def create_tables():
    """Create all the database tables."""
    database.create_all()


@app.cli.command('drop_tables')
def drop_tables():
    """Drop all the database tables."""
    database.drop_all()


##########
# Routes #
##########

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/async_get_urls')
@timer
def async_get_urls():
    sites = asyncio.run(get_all_urls())

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"

    return response


@app.route('/async_list_urls')
@timer
def async_list_urls():
    results = asyncio.run(async_get_sites())

    # Generate the HTML response
    response = '<h1>Database Items</h1>'
    response += f"<p># of Sites: {results['sites_count']}, # of Valid Sites: {results['sites_valid_count']}</p>"
    for site in results['sites'].all():
        response += f"<p>index: {site[0].id} --- URL: {site[0].url} --- Status Code: {site[0].status}</p>"

    return response
