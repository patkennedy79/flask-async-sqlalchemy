## Overview

This Flask application demonstrates how to utilize `async`/`await` to make asynchronous calls using [asyncio](https://docs.python.org/3/library/asyncio.html), [aiohttp](https://docs.aiohttp.org/en/stable/), and [SQLAlchemy](https://docs.sqlalchemy.org/en/14/) (asynchronous capability added in v1.4).

After starting the Flask development server, navigating to 'http://localhost:5000/async_get_urls' results in 10 asynchronous GET calls via `aiohttp` and the results are saved to the Postgres database using the new asynchronous capability in SQLAlchemy v1.4:

![Flask Async SQLAlchemy - Get URLs](static/img/flask_async_get_urls.png?raw=true "Flask Async SQLAlchemy - Get URLs")

Navigating to 'http://localhost:5000/async_get_urls' results in an asynchronous call via SQLAlchemy v1.4 to retrieve all items from the Postgres database:

![Flask Async SQLAlchemy - List from Database](static/img/flask_async_list.png?raw=true "Flask Async SQLAlchemy - List from Database")

## PostgreSQL Database

The easiest approach to setting up a PostgreSQL database is to utilize Docker: [PostgreSQL - DockerHub](https://hub.docker.com/_/postgres)

Pull down the latest stable image of Postgres:

```sh
docker pull postgres
```

Create a new directory to act as the host mount for the PostgreSQL data files:

```sh
mkdir -p ~/Docker/volumes/postgres
```

Run the PostgreSQL container (as a daemon service) to serve the PostgreSQL database:

```sh
docker run --rm --name flaskdb -e POSTGRES_USER=flask -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=flaskdb -d -p 5432:5432 -v ~/Docker/volumes/postgres3:/var/lib/postgresql/data postgres
```

Key environment variables used in the PostgreSQL container:

* POSTGRES_USER - creates the specified user with superuser power 
* POSTGRES_PASSWORD - sets the superuser password for PostgreSQL (**REQUIRED!**)
* POSTGRES_DB - defines a different name for the default database that is created when the image is first started

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask-async-sqlalchemy.git
```

Create a new virtual environment:

```sh
$ cd flask-async-sqlalchemy
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

Set the file that contains the Flask application and specify that the development environment should be used:

```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://localhost:5000' to confirm the Flask configuration (Hello World! should be displayed).

## Key Python Modules Used
 
* Flask: micro-framework for web application development
* SQLAlchemy - ORM (Object Relational Mapper)
* asycio
* aiohttp

This application is written using Python 3.9.2.

## Testing

To run all the tests:

```sh
(venv) $ pytest -v
```

To check the code coverage of the tests:

```sh
(venv) $ pytest --cov-report term-missing --cov=app
```
