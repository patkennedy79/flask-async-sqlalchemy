"""SQLAlchemy Data Models."""
from sqlalchemy import Column
from sqlalchemy.types import Integer, Text, String
from database_async import AsyncDatabase


database = AsyncDatabase('postgresql+asyncpg://flask:docker@localhost:5432/flaskdb')


class User(database.model):
    """User account."""

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    username = Column(String(255), unique=True, nullable=False)
    password = Column(Text, nullable=False)

    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password

    def __repr__(self):
        return "<User %r>" % self.username


class Site(database.model):
    __tablename__ = 'sites'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    url = Column(String(255), nullable=False)
    status = Column(Integer, nullable=False)

    def __init__(self, url: str, status: int):
        self.url = url
        self.status = status

    def __repr__(self):
        return f'<Site {self.url}>'
