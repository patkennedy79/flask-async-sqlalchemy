from flask import Flask
import asyncio
from aiohttp import ClientSession
from ssl import create_default_context
from certifi import where
from threading import current_thread
import requests


# Create the Flask application
app = Flask(__name__)

# Create a SSL context to use with aiohttp
sslcontext = create_default_context(cafile=where())

# List of (10) websites to access in this script
urls = ['https://www.kennedyrecipes.com',
        'https://www.kennedyrecipes.com/breakfast/pancakes/',
        'https://www.kennedyrecipes.com/breakfast/honey_bran_muffins/',
        'https://www.kennedyrecipes.com/breakfast/acai_bowl/',
        'https://www.kennedyrecipes.com/breakfast/breakfast_scramble/',
        'https://www.kennedyrecipes.com/breakfast/pumpkin_donuts/',
        'https://www.kennedyrecipes.com/breakfast/waffles/',
        'https://www.kennedyrecipes.com/breakfast/omelette/',
        'https://www.kennedyrecipes.com/baked_goods/bagels/',
        'https://www.kennedyrecipes.com/baked_goods/irish_soda_bread/']

print(f"Global scope of Flask application: {current_thread().name}")


####################
# Helper Functions #
####################

async def fetch_url(session, url):
    """Fetch the specified URL using the aiohttp session specified."""
    response = await session.get(url, ssl=sslcontext)
    return {'url': response.url, 'status': response.status}


async def get_all_urls():
    """Retrieve the list of URLs."""

    # Retrieve all the URLs asynchronously using aiohttp
    async with ClientSession() as session:
        tasks = []
        for url in urls:
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        results = await asyncio.gather(*tasks)

    return results


##########
# Routes #
##########

@app.route('/async_get_all_urls')
def async_get_all_urls():
    print(f'Inside async_get_all_urls(): {current_thread().name}')
    sites = asyncio.run(get_all_urls())

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"
    return response


@app.route('/sync_get_all_urls')
def sync_get_all_urls():
    sites = []
    for url in urls:
        response = requests.get(url)
        sites.append({'url': response.url, 'status': response.status_code})

    # Generate the HTML response
    response = '<h1>URLs:</h1>'
    for site in sites:
        response += f"<p>URL: {site['url']} --- Status Code: {site['status']}</p>"
    return response
