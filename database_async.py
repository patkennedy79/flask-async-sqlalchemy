"""Database engine & session creation."""
import asyncio
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData
from sqlalchemy.pool import NullPool


####################
# Helper Functions #
####################

async def async_database_connection(uri):
    engine = create_async_engine(uri, echo=True, poolclass=NullPool)
    Session = sessionmaker(bind=engine, expire_on_commit=False, class_=AsyncSession)
    session = Session()
    return engine, session


async def create_all_tables(engine, model):
    """Create all the tables defined in models.py using SQLAlchemy."""

    # `create_all()` must be run using a synchronous connection
    async with engine.begin() as conn:
        await conn.run_sync(model.metadata.create_all)


async def drop_all_tables(engine, model):
    """Drop all the tables defined in models.py using SQLAlchemy."""

    # `drop_all()` must be run using a synchronous connection
    async with engine.begin() as conn:
        await conn.run_sync(model.metadata.drop_all, checkfirst=True)


###################################
# Asynchronous Database Interface #
###################################

class AsyncDatabase:
    """Asynchronous SQLAlchemy interface to the database."""

    def __init__(self, database_uri):
        self.uri = database_uri
        self.engine, self.session = asyncio.run(async_database_connection(database_uri))
        self.model = declarative_base()
        self.metadata = MetaData()
        self.metadata.bind = self.engine

    def create_all(self):
        asyncio.run(create_all_tables(self.engine, self.model))

    def drop_all(self):
        asyncio.run(drop_all_tables(self.engine, self.model))

    def __repr__(self):
        return f'Async database interface for {self.database_uri}.'
