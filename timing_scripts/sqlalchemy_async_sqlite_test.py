import asyncio

from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker
from random import randrange
from timeit import default_timer


Base = declarative_base()


class User(Base):
    """User account."""

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    username = Column(String(255), unique=True, nullable=False)
    password = Column(String(255), nullable=False)

    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password

    def __repr__(self):
        return f'<User {self.username}>'


async def async_main():
    engine = create_async_engine(
        "sqlite+aiosqlite:///./test.db",
        # echo=True,
    )

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    # expire_on_commit=False will prevent attributes from being expired
    # after commit.
    async_session = sessionmaker(
        engine, expire_on_commit=False, class_=AsyncSession
    )

    users = []
    random_base = randrange(0, 1000000, 100)
    for index in range(1000):
        users.append(User('user' + str(random_base+index), 'password' + str(random_base+index)))

    start_time = default_timer()

    async with async_session() as session:
        async with session.begin():
            session.add_all(users)
        result = await session.execute(select(User).order_by(User.id))
        await session.commit()

    tot_elapsed = default_timer() - start_time
    print(f'Elapsed time: {tot_elapsed}')
    print(f'Users added to database: {len(result.all())}')


asyncio.run(async_main())
