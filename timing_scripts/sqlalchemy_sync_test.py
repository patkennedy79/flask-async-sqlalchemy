from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker
from random import randrange
from timeit import default_timer


Base = declarative_base()


class User(Base):
    """User account."""

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    username = Column(String(255), unique=True, nullable=False)
    password = Column(String(255), nullable=False)

    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password

    def __repr__(self):
        return f'<User {self.username}>'


def sync_main():
    engine = create_engine(
        'postgresql://flask:docker@localhost:5432/flaskdb',
        # echo=True,
    )

    Session = sessionmaker(bind=engine)
    session = Session()

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    start_time = default_timer()

    random_base = randrange(0, 1000000, 100)
    for index in range(1000):
        session.add(User('user' + str(random_base+index), 'password' + str(random_base+index)))
    result = session.execute(select(User).order_by(User.id))
    session.commit()

    tot_elapsed = default_timer() - start_time
    print(f'Elapsed time: {tot_elapsed}')
    print(f'Users added to database: {len(result.all())}')


sync_main()
